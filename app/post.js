const PostPageController = ($stateParams, $scope, PostService, CommentFactory, $timeout) => {
  let post = $stateParams.post 
  let commentService = new CommentFactory($stateParams.id);

  const setListState = () => {
    console.log('list data is ', commentService.list);
    $timeout(() => $scope.comments = commentService.list);
  };

  commentService.db.info().then(result => {
    $timeout(() => $scope.commentCount = result.doc_count);
  });

  if (!post)  {
    PostService
      .getPost($stateParams.id)
      .then(result => $timeout(() => $scope.post = result));
  } else {
    $scope.post = post;
  }

  $scope.addComment = (comment) => {
    PostService.postComment($stateParams.id, { text: comment }).then(result => console.log('comment added ', result));
  };

  // fetch first set of comments
  commentService
    .getAll()
    .then((result) => {
      setListState(result);
      commentService.startListening();
    });

  // Done. Stop listening to comments from server
  $scope.$on('$destroy', commentService.stopListening);
};
PostPageController.$inject = ['$stateParams', '$scope', 'PostService', 'CommentFactory', '$timeout']

const PostPageTemplate = `
  <div class="post-wrapper">
    <div>
      <a class="post-link" target="_blank" href="{{ post.link }}">
        {{ post.title }}
      </a>
      <a class="post-comments-link" ui-sref="post({ id: post._id, post: post })">{{ commentCount }} comments</a>
    </div>

    <new-comment on-add="addComment(comment)"></new-comment>

    <comment 
      post-id="post._id"
      doc="comment"
      ng-repeat="comment in comments">
    </comment>
  </div>
`;

angular
  .module('hNews')
  .component('postPage', {
    controller: PostPageController,
    template: PostPageTemplate,
    bindings: {
      //TODO: posts bindings
    }
  })
  .config(($stateProvider) => {
    $stateProvider.state({
      name: 'post',
      url: '/:id',
      component: 'postPage',
      params: {
        post: null
      }
    })
  });