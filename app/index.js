var hNews = angular.module('hNews', [ 'ui.router' ]);

const hNewsController = ($scope, UserService, $rootScope) => {
  $scope.loggedInUser = UserService.user;

  $scope.logout = function () {
    UserService.logout();
    $scope.loggedInUser = null;
  };

  $rootScope.$on('User changed', function () {
    $scope.loggedInUser = UserService.user;
  })
};
hNewsController.$inject = [ '$scope', 'UserService', '$rootScope' ];


hNews
  .constant('API_URL', window.CONFIG.COUCHDB_URL || 'http://127.0.0.1:4000/v1')
  .constant('COUCHDB_URL', window.CONFIG.COUCHDB_URL || 'http://127.0.0.1:5984')
  .controller('HNewsController', hNewsController);