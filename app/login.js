const LoginPageController = ($scope, UserService, $state) => {
  $scope.login = function (formData) {
    UserService
      .login(formData)
      .then(result => $state.go('home', {}, { reload: true }));
  };

  $scope.register = function (formData) {
    console.log('reaching here....');
    UserService
      .register(formData)
      .then(user => {
        $state.go('home', {}, { reload: true });
      });
  };
};
LoginPageController.$inject = ['$scope', 'UserService', '$state']

const LoginPageTemplate = `
  <div class="login-wrapper">
    <form name='loginForm' ng-submit="login(formData)">
      <input type="text" placeholder="Username" ng-model="formData.username" required />
      <input type="password" placeholder="Password" ng-model="formData.password" required />
      <button type='submit' ng-disabled="loginForm.$invalid">Login</button>
      Or 
      <button 
        type="button" 
        ng-click="register(formData)" 
        ng-disabled="loginForm.$invalid">Register</button>
    </form>
  </div>
`;

angular
  .module('hNews')
  .component('loginPage', {
    controller: LoginPageController,
    template: LoginPageTemplate,
    bindings: {
      //TODO: logins bindings
    }
  })
  .config(($stateProvider) => {
    $stateProvider.state({
      name: 'login',
      url: '/login',
      component: 'loginPage'
    })
  });