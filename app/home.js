const HomePageController = (PostService, $timeout, $scope, UserService) => {


  const setListState = () => {
    $timeout(() => $scope.posts = PostService.list);
  }

  $scope.createPost = (newPost) => {
    PostService.createPost(newPost).then(() => {
      $scope.newPost = {};
      $scope.showCreateForm = false;
      alert('New post created successfully');
    });
  }

  !PostService.list.length ? PostService.fetchPosts().then(setListState) : setListState();

};
HomePageController.$inject = [ 'PostService', '$timeout', '$scope', 'UserService' ]

const HomePageTemplate = `
  <div>
    <form name="newPostForm" ng-submit="createPost(newPost)" class="new-post-wrapper">
      <a class="new-post-link" role="button" ng-click="showCreateForm = true" ng-hide="showCreateForm">
        + New Post
      </a>
      <div ng-show="showCreateForm">
        <input type="text" required placeholder="http://" ng-model="newPost.link" />
        <input type="text" required placeholder="title" ng-model="newPost.title" />

        <button type="submit" ng-disabled="newPostForm.$invalid">create</button>
        <a role="button" ng-click="showCreateForm = false">cancel</a>
      </div>
    </form>
    <div ng-repeat="item in posts" class="post-wrapper">
      <a class="post-link" target="_blank" href="{{ item.doc.link }}">
        {{ $index + 1 }}.
        {{ item.doc.title }}
      </a>
      <a class="post-comments-link" ui-sref="post({ id: item.doc._id, post: item.doc })">comments</a>
    </div>
  </div>
`;

angular
  .module('hNews')
  .component('homePage', {
    controller: HomePageController,
    controllerAs: 'vm',
    template: HomePageTemplate,
    bindings: {
      //TODO: posts bindings
    }
  })
  .config(($stateProvider) => {
    $stateProvider.state({
      name: 'home',
      url: '/',
      default: true,
      component: 'homePage'
    })
  });