
const CommentController = ($scope, CommentFactory) => {
  var ctrl = $scope.$ctrl;
  ctrl.toggleReply = false;


  $scope.handleReply = function (comment) {
    CommentFactory
      .reply(ctrl.postId, comment, ctrl.doc._id)
      .then(() => ctrl.toggleReply = false);
  };

  $scope.handleEdit = function (comment, id) {
    CommentFactory
      .edit(ctrl.postId, comment, id)
      .then(() => ctrl.toggleEdit = false);
  }
};
CommentController.$inject = ['$scope', 'CommentFactory'];



angular
  .module('hNews')
  .component('newComment', {
    templateUrl: '/components/new-comment.html',
    controller: CommentController,
    bindings: {
      onAdd: '&',
      comment: '<?'
    }
  })
  .component('comment', {
    templateUrl: '/components/comment.html',
    controller: CommentController,
    bindings: { 
      doc: '=',
      postId: "="
    }
  });