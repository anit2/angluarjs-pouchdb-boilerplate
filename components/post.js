
const PostController = () => {
  let vm = this;
};
PostController.$inject = [];


angular
  .module('hNews')
  .component('post', {
    templateUrl: '/components/post.html',
    controller: PostController,
    bindings: { }
  });