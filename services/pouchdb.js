const UserService = function (API_URL, $http, $rootScope, $window) {
  const svc = this;
  this.user = null;

  try {
    this.user = JSON.parse(localStorage.getItem('user'));
  } catch(e) {}

  const setUser = (user) => {
    $window.localStorage && $window.localStorage.setItem('user', JSON.stringify(user.data));
    svc.user = user.data;
    $rootScope.$broadcast('User changed');
    return svc.user;
  }

  this.login = (formData) => {
    return $http.post(`${API_URL}/user/session`, formData).then(setUser);
  };

  this.logout = () => {
    $window.localStorage && $window.localStorage.removeItem('user');
    this.user = null;
    $rootScope.$broadcast('User changed');
  };

  this.getAuthHeaders = () => this.user ? { 'AuthToken': this.user.token } : null

  this.register = (formData) => $http
    .post(`${API_URL}/user/register`, formData)
    .then(setUser);
};

UserService.$inject = [ 'API_URL', '$http', '$rootScope', '$window' ];


const PouchHelpers = function () {
  this.makeListFetcher = function (service) {
    service.options = { limit: 20, include_docs: true };
    service.list = [];
    service.fetchList = () => {
      return service.db
        .allDocs(service.options, (err, response) => {
          // TODO: Handle error
          if(err) return console.log('error is ', err);
  
          service.list = [...service.list, ...response.rows];
          service.options.startkey = response.rows[response.rows.length - 1].id;
          service.options.skip = 1;
        });
    };
  };

  this.flatToTree = (docs, parentAttrName = 'parent') => {
    const arrMap = new Map(docs.map(x => [x._id, x]));
    const finalArr = [];

    docs.forEach(item => {
      if (item[parentAttrName]) {
        let parentItem = arrMap.get(item[parentAttrName]);
        parentItem.children = [ ...(parentItem.children || []), item];
      } else {
        finalArr.push(item)
      }
    })
    return finalArr;
  };
}

angular
  .module('hNews')
  .service('UserService', UserService)
  .service('PouchHelpers', PouchHelpers);