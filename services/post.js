// Global Post service. Sigleton because the data is home page worthy.

const PostService = function (COUCHDB_URL, API_URL, $http, UserService) {
  let service = this;
  let db = new PouchDB(`${COUCHDB_URL}/posts`);
  service.options = { limit: 5, include_docs: true }

  service.list = [];

  // Fetch posts and append to list, paginated obviously!
  service.fetchPosts = () => {
    return db
      .allDocs(service.options, (err, response) => {
        // TODO: Handle error
        if(err) return console.log('error is ', err);

        service.list = [...service.list, ...response.rows];
        service.options.startkey = response.rows[response.rows.length - 1].id;
        service.options.skip = 1;
      });
  };



  service.createPost = (newPost) => $http
    .post(`${API_URL}/posts`, newPost, { 
      headers: UserService.getAuthHeaders() 
    });

  service.getPost = (id) => db.get(id);
  
  service.postComment = (id, comment) => $http
    .post(`${API_URL}/posts/${id}/comment`, comment, { 
      headers: UserService.getAuthHeaders() 
    });
};
PostService.$inject = [ 'COUCHDB_URL', 'API_URL', '$http', 'UserService' ];

angular
  .module('hNews')
  .service('PostService', PostService)
