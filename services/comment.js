const remoteOptions = {
  live: true,
  retry: true,
  continuous: true
};


const CommentFactory = function (COUCHDB_URL, $http, API_URL, PouchHelpers, UserService) {  
  let CommentFactory = function (postId) {
    if (!postId) throw 'Please provide a post id to create a comment factory';

    this.postId = postId;
    this.db = new PouchDB(`post_comments_${this.postId}`);
    this.list = [];
  };

  CommentFactory.prototype.stopListening = function () {
    if (!this.listener) throw 'No listener found to stop.';

    this.listener.cancel();
  };

  CommentFactory.prototype.getAll = function () {
    return this
      .db
      .allDocs({ include_docs: true })
      .then(result => {
        this.list = PouchHelpers.flatToTree(result.rows.map(x => x.doc));
      });
  }

  CommentFactory.prototype.startListening = function () {
    if (this.listener) throw 'Already listening for comments.';

    this.listener = this.db.replicate
      .from(`${COUCHDB_URL}/post_comments_${this.postId}`, {
        since: 'now',
        include_docs: true,
        live: true,
        continuous: true,
        retry: true
      });

    this.listener.on('change', changes => console.log('changes are ....', `post_comments_${this.postId}`, changes));
  };

  CommentFactory.reply = function (postId, comment, parentCommentId) {
    return $http.post(`${API_URL}/comments/${parentCommentId}/reply`, {
      text: comment,
      post_id: postId
    }, {
      headers: UserService.getAuthHeaders() 
    });
  };

  CommentFactory.edit = function (postId, comment, commentId) {
    return $http.put(`${API_URL}/comments/${commentId}`, {
      text: comment,
      post_id: postId
    }, {
      headers: UserService.getAuthHeaders() 
    });
  }

  return CommentFactory;
};
CommentFactory.$inject = [ 'COUCHDB_URL', '$http', 'API_URL', 'PouchHelpers', 'UserService' ];

angular
  .module('hNews')
  .factory('CommentFactory', CommentFactory)
